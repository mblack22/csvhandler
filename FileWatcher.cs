﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Security.Permissions;

namespace CSVHandler
{
    public class FileWatcher
    {
        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        public static void Run()
        {
            string[] args = System.Environment.GetCommandLineArgs();
            if (args.Length != 2)
            {
                Console.WriteLine("Enter a valid directory to watch");
                return;
            }

            FileSystemWatcher watcher = new FileSystemWatcher();

            watcher.Path = args[1];
            Console.WriteLine(args[1]);

            // Watch both files and subdirectories.
            watcher.IncludeSubdirectories = true;

            watcher.NotifyFilter = NotifyFilters.Attributes |
            NotifyFilters.CreationTime |
            NotifyFilters.DirectoryName |
            NotifyFilters.FileName |
            NotifyFilters.LastAccess |
            NotifyFilters.LastWrite |
            NotifyFilters.Security |
            NotifyFilters.Size;

            // Watch all files.
            watcher.Filter = "*.txt*";

            // Add event handlers.
            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnChanged);
            watcher.Renamed += new RenamedEventHandler(OnChanged);

            //Start monitoring.
            watcher.EnableRaisingEvents = true;
 
        }

        public static void OnChanged(object sender, FileSystemEventArgs e)
        {
            string eventType = e.ChangeType.ToString();
            if (eventType == "Created")
            {
                //do something
                
                ProcessFile(e.FullPath);
            }
        }

        private static void OnRenamed(object source, RenamedEventArgs e)
        {
            // Specify what is done when a file is renamed.
            Console.WriteLine("File: {0} renamed to {1}", e.OldFullPath, e.FullPath);
        }

        public static bool ProcessFile(string pPath)
        {
            //this will have to be thread aware?  http://msdn.microsoft.com/en-us/library/system.io.filesystemwatcher%28VS.85%29.aspx
            //copy file from main folder to sandbox

            //call FileImportEngine to insert rows in table

           // FileImportEngine fie = new FileImportEngine();
           // Archive[] theFileRows = fie.FileItems(pPath);
            Console.WriteLine(pPath + " found!");
            //fie.RecordRows(theFileRows);

            return true;
        }
    }
}
