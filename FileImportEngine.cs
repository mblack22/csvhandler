﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using FileHelpers;


namespace CSVHandler
{
    public class FileImportEngine
    {

        public FileImportEngine()
        {
            
        }

        public Archive[] FileItems(string pFileName)
        { 
            FileHelperEngine engine = new FileHelperEngine(typeof(Archive));
            Archive[] archive = engine.ReadFile(pFileName) as Archive[];
            return archive;
        }
        public void RecordRows(Archive[] pArchive)
        {

            ConsolePrintRows(pArchive);
            CSVHandler.Database db = new Database();
            db.insertRows(pArchive);
        }

        [Conditional("DEBUG")] 
        public void ConsolePrintRows(Archive[] pArchive)
        {
            foreach (Archive arch in pArchive)
            {

                Console.WriteLine(arch.getComputerName() + " " + arch.getIPAddress() + " " + arch.getLastReportTime());

            }
        }
    }
}
