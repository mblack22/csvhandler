﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

namespace CSVHandler
{
    [DelimitedRecord(",")]
    [IgnoreFirst(1)]
    public class Archive
    {
        private string computerName;
        private string ipAddress;
        private string lastReportTime;    
        
        public String getComputerName()
        {
             return computerName;
        }
        public void setComputerName(string pComputerName)
        {
            computerName = pComputerName;
        }
        public String getIPAddress()
        {
            return ipAddress;
        }
        public void setIPAddress(string pIPAddress)
        {
            ipAddress = pIPAddress;
        }
        public string getLastReportTime()
        {
            return lastReportTime;
        }
        public void setLastReportTime(string pLastReportTime)
        {
            lastReportTime = pLastReportTime;
        }
    }


}
