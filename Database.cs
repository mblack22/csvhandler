﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using System.Configuration;

namespace CSVHandler
{
    class Database
    {

        public void insertRows(Archive[] archive)
        {
            using (var conn = new OracleConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Connection"].ToString()))
            {
                
                conn.Open();

                var cmd = conn.CreateCommand();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "DEVICE_INSERT";
                var deviceName = cmd.Parameters.Add("device_name", OracleDbType.Varchar2, System.Data.ParameterDirection.Input);
                var ipAddress = cmd.Parameters.Add("ip_address", OracleDbType.Varchar2, System.Data.ParameterDirection.Input);
                var lastReportTime = cmd.Parameters.Add("last_report_time", OracleDbType.Varchar2, System.Data.ParameterDirection.Input);
                string[] dNames = null;
                string[] ipAddys = null;
                string[] times = null;
                int count = 0;
                dNames = new string[archive.Length];
                ipAddys = new string[archive.Length];
                times = new string[archive.Length];
                foreach (Archive arch in archive)
                {

                    dNames[count] = arch.getComputerName();
                    ipAddys[count] = arch.getIPAddress();
                    times[count] = arch.getLastReportTime();
                    count =  count + 1;
                    
                }
              
                deviceName.Value = dNames;
                ipAddress.Value = ipAddys;
                lastReportTime.Value = times;

                cmd.ArrayBindCount = archive.Length;
                cmd.ExecuteNonQuery();

            }
        }
    }
}
